package com.folcademy.clinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableResourceServer
@SpringBootApplication
public class ClinicaApplication {
    //Hola soy un comentario para que funcione el pipeline 
    public static void main(String[] args) {
        SpringApplication.run(ClinicaApplication.class, args);
    }

}
