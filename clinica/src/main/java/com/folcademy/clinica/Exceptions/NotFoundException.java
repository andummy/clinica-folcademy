package com.folcademy.clinica.Exceptions;

import lombok.Data;

@Data
public class NotFoundException extends RuntimeException{

    private String args;

    public NotFoundException(String message,String args) {
        super(message);
        this.args = args;
    }
}
