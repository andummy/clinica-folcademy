package com.folcademy.clinica.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    //Consultar Si resulta bueno devolver la excepcion
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiExceptionResponse badRequestException(HttpServletRequest req, RuntimeException exception){
        return new ApiExceptionResponse(HttpStatus.BAD_REQUEST, exception.getMessage()/*, exception,*/, "",req.getRequestURI());
    }


    //Consultar Si resulta bueno devolver la excepcion
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ApiExceptionResponse NotFoundException(HttpServletRequest req, NotFoundException exception){
        System.out.println(exception.getArgs());
        return new ApiExceptionResponse(HttpStatus.NOT_FOUND, exception.getMessage(), exception.getArgs(),req.getRequestURI());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    //Consultar sobre formateo del mensaje de la excepcion
    public ApiExceptionResponse MethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException exception){
        return new ApiExceptionResponse(HttpStatus.BAD_REQUEST, exception.getMessage(), exception.getFieldError().getField(),req.getRequestURI());
    }
}
