package com.folcademy.clinica.Exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public class ApiExceptionResponse {

    private HttpStatus status;
    private String message;
    private String args;
    private String path;
    //private RuntimeException exceptions;

    public ApiExceptionResponse(HttpStatus status, String message/*, RuntimeException exceptions*/, String args,String path) {
        this.status = status;
        this.message = message;
        this.args = args;
        this.path = path;
        //this.exceptions = exceptions;
    }
}
