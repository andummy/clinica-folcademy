package com.folcademy.clinica.Exceptions;

public class ErrorMessages {
    public static final String USUARIO_CODIGO_NOT_FOUND = "El usuario no existe, COD_NOT_FOUND";

    public static final String USUARIO_DESC_NOT_FOUND = "El usuario no existe, DESC_NOT_FOUND";

    public static  final String MEDICO_NOT_FOUND = "El medico no existe";

    public static  final String PACIENTE_NOT_FOUND = "El paciente no existe";

    public static  final String TURNO_NOT_FOUND = "El turno no existe";
}
