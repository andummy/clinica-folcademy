package com.folcademy.clinica.Exceptions;

import lombok.Data;

@Data
public class BadRequestException extends RuntimeException{

    private String args;

    public BadRequestException(String message,String args) {
        super(message);
        this.args = args;
    }
}
