package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDtoA;
import com.folcademy.clinica.Model.Dtos.TurnoDtoBM;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @PreAuthorize("#oauth2.hasScope('Read') and hasAuthority('turnos')")
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<TurnoDtoBM> findAll(){
        return this.turnoService.findAll();
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('turnos')")
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public TurnoDtoBM createTurno(@RequestBody @Validated TurnoDtoA turnoDto){
        return this.turnoService.createTurno(turnoDto);
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('turnos')")
    @PostMapping("/update/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public TurnoDtoBM updateTurno(@RequestBody @Validated TurnoDtoA turnoDto, @PathVariable(name = "id") Integer idTurno){
        return this.turnoService.updateTurno(turnoDto, idTurno);
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('turnos')")
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity deleteTurno(@PathVariable(name = "id") Integer idTurno){
        return this.turnoService.deleteTurno(idTurno);
    }
}
