package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDtoABM;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @PreAuthorize("#oauth2.hasScope('Read') and hasAuthority('medicos')")
    @GetMapping(value = "")
    @ResponseBody
    public List<MedicoDtoABM> findAll() {
        return this.medicoService.findAllMedicos();
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('medicos')")
    @PostMapping(value = "")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Medico createMedico(@RequestBody @Validated MedicoDtoABM dto){
        return this.medicoService.createMedico(dto);
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('medicos')")
    @PostMapping(value = "/update/{idmedico}")
    @ResponseBody
    public Medico updateMedico(@RequestBody @Validated MedicoDtoABM dto, @PathVariable(name = "idmedico") Integer idmedico){
        return this.medicoService.updateMedico(dto, idmedico);
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('medicos')")
    @DeleteMapping(value = "/delete/{idmedico}")
    @ResponseBody
    public ResponseEntity deleteMedico(@PathVariable(name = "idmedico") Integer idmedico){
        //Consultar si el block try-catch debera de ir dentro del controller o del service
        return medicoService.deleteMedico(idmedico);
    }
}
