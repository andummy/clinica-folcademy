package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDtoABM;
import com.folcademy.clinica.Model.Dtos.PacienteDtoABM;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PreAuthorize("#oauth2.hasScope('Read') and hasAuthority('pacientes')")
    @GetMapping(value = "")
    @ResponseBody
    public List<PacienteDtoABM> findAll() {
        return this.pacienteService.findAllPacientes();
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('pacientes')")
    @PostMapping(value = "")
    @ResponseBody
    public  Paciente createPaciente(@RequestBody @Validated PacienteDtoABM dto){
        return this.pacienteService.createPaciente(dto);
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('pacientes')")
    @PostMapping(value = "/update/{idpaciente}")
    @ResponseBody
    public Paciente updatePaciente(@RequestBody @Validated PacienteDtoABM dto, @PathVariable(name = "idpaciente") Integer idpaciente){
        return this.pacienteService.updatePaciente(dto, idpaciente);
    }

    @PreAuthorize("#oauth2.hasScope('Write') and hasAuthority('pacientes')")
    @DeleteMapping("/delete/{idpaciente}")
    @ResponseBody
    public  ResponseEntity deletePaciente(@PathVariable(name = "idpaciente") Integer idpaciente){
        return this.pacienteService.deletePaciente(idpaciente);
    }
}
