package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDtoABM;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PacienteMapper {
    private final ModelMapper modelMapper = new ModelMapper();

    public PacienteDtoABM entityToDto(Paciente entity){
        return  modelMapper.map(entity, PacienteDtoABM.class);
    }

    public Paciente dtoToEntity(PacienteDtoABM dto){
        return modelMapper.map(dto, Paciente.class);
    }

    public void updateEntityFromDto(PacienteDtoABM dto, Paciente entity){
        entity.setDni(dto.getDni());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
    }
}
