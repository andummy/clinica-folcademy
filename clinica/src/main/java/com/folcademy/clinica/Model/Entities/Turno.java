package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "turno", indexes = {
        @Index(name = "fk_paciente_idx", columnList = "idpaciente"),
        @Index(name = "fk_medico_idx", columnList = "idmedico")
})
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", nullable = false)
    private Integer id;

    @Column(name = "fecha", nullable = false)
    private LocalDate fecha;

    @Column(name = "hora", nullable = false)
    private LocalTime hora;

    @Column(name = "atendido", columnDefinition = "TINYINT")
    private Integer atendido;

    @ManyToOne
    @JoinColumn(name = "idpaciente")
    private Paciente idpaciente;

    @ManyToOne
    @JoinColumn(name = "idmedico")
    private Medico idmedico;
}