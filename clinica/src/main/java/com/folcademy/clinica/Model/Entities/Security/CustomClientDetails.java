package com.folcademy.clinica.Model.Entities.Security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.*;
import java.util.stream.Collectors;

public class CustomClientDetails implements ClientDetails {

    private final Usuario usuario;

    public CustomClientDetails(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String getClientId() {
        return usuario.getUsuarioDesc();
    }

    @Override
    public Set<String> getResourceIds() {
        return usuario.getResourceSet()
                .stream().map(
                        resource -> resource.getName()
                ).collect(Collectors.toSet());
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {

        return usuario.getUsuarioClave();
    }

    @Override
    public boolean isScoped() {
        return usuario.getScopeSet().size() != 0;
    }

    @Override
    public Set<String> getScope() {
        return usuario.getScopeSet()
                .stream().map(
                        scope -> scope.getName()
                ).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return usuario.getAuthorizedGrantTypesSet()
                .stream().map(
                        authorizedGrantTypes -> authorizedGrantTypes.getName()
                ).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return usuario.getAuthoritySet()
                .stream().map(
                        authority -> new SimpleGrantedAuthority(authority.getName())
                ).collect(Collectors.toList());
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return usuario.getAccesstokenvalidityseconds();
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return usuario.getRefreshtokenvalidityseconds();
    }

    @Override
    public boolean isAutoApprove(String s) {
        return true;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }
}
