package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDtoABM {
    @NotBlank(message = "Dni is mandatory")
    @Pattern(regexp = "^[\\d]{1,3}\\.?[\\d]{3}\\.?[\\d]{3}$", message = "Invalid Dni")
    String dni;

    @NotBlank(message = "Nombre is mandatory")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Invalid Nombre")
    String nombre;

    @NotBlank(message = "Apellido is mandatory")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Invalid Apellido")
    String apellido;

    @NotBlank(message = "Telefono is mandatory")
    String telefono;
}
