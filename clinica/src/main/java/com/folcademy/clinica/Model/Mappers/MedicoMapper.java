package com.folcademy.clinica.Model.Mappers;
import com.folcademy.clinica.Model.Dtos.MedicoDtoABM;
import com.folcademy.clinica.Model.Entities.Medico;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class MedicoMapper {
    private final ModelMapper modelMapper = new ModelMapper();

    public MedicoDtoABM entityToDto(Medico entity){
        return  modelMapper.map(entity, MedicoDtoABM.class);
    }

    public Medico dtoToEntity(MedicoDtoABM dto){
        return modelMapper.map(dto, Medico.class);
    }

    public void updateEntityFromDto(MedicoDtoABM dto, Medico entity){
        entity.setConsulta(dto.getConsulta());
        entity.setApellido(dto.getApellido());
        entity.setProfesion(dto.getProfesion());
        entity.setNombre(dto.getNombre());
    }
}
