package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "medico")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED", nullable = false)
    private Integer id;

    @Column(name = "nombre", columnDefinition = "VARCHAR" ,nullable = false, length = 45)
    private String nombre;

    @Column(name = "apellido", columnDefinition = "VARCHAR", nullable = false, length = 45)
    private String apellido;

    @Column(name = "profesion", columnDefinition = "VARCHAR", length = 45)
    private String profesion;

    @Column(name = "consulta", columnDefinition = "INT")
    private Integer consulta;
}