package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDtoABM {

    @NotBlank(message = "Nombre is mandatory")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Invalid Nombre")
    String nombre;

    @NotBlank(message = "Apellido is mandatory")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Invalid Nombre")
    String apellido;

    @NotBlank(message = "Profesion is mandatory")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Invalid Nombre")
    String profesion;
    Integer consulta;
}
