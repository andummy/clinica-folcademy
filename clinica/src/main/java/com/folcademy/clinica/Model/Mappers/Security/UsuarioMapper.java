package com.folcademy.clinica.Model.Mappers.Security;

import com.folcademy.clinica.Model.Entities.Security.Usuario;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.User;

import java.util.stream.Collectors;

@Component("usuarioMapper")
public class UsuarioMapper {

    public User usuarioEntityToSpringSecurity(Usuario usuario) {
        return new User(
                usuario.getUsuarioDesc(),
                usuario.getUsuarioClave(),
                usuario.getUsuarioDelete().equals(0),
                true,
                true,
                true,
                usuario
                        .getAuthoritySet().stream()
                        .map(
                                authority -> new SimpleGrantedAuthority(authority.getName())
                        ).collect(Collectors.toList())
        );
    }
}
