package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDtoBM {
    @NotBlank(message = "Fecha is mandatory")
    private LocalDate fecha;

    @NotBlank(message = "Hora is mandatory")
    private LocalTime hora;

    MedicoDtoABM medico;

    PacienteDtoABM paciente;
}
