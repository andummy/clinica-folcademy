package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDtoA {

    @NotNull(message = "Fecha is mandatory")
    private LocalDate fecha;

    @NotNull(message = "Hora is mandatory")
    private LocalTime hora;

    @NotNull(message = "IdMedico is mandatory")
    Integer idMedico;

    @NotNull(message = "IdPaciente is mandatory")
    Integer idPaciente;
}
