package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDtoA;
import com.folcademy.clinica.Model.Dtos.TurnoDtoBM;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TurnoMapper {
    private final ModelMapper modelMapper = new ModelMapper();
    private final MedicoMapper medicoMapper;
    private final PacienteMapper pacienteMapper;

    public TurnoMapper(MedicoMapper medicoMapper, PacienteMapper pacienteMapper) {
        this.medicoMapper = medicoMapper;
        this.pacienteMapper = pacienteMapper;
    }

    public Turno dtoAToEntity(TurnoDtoA dtoA, Paciente pacienteEntity, Medico medicoEntity){
        Turno turno = new Turno();
        turno.setIdpaciente(pacienteEntity);
        turno.setIdmedico(medicoEntity);
        turno.setFecha(dtoA.getFecha());
        turno.setHora(dtoA.getHora());
        turno.setAtendido(0);
        return turno;
    }

    public Turno dtoToEntity(TurnoDtoBM DtoBM){
        return null;
    }

    public TurnoDtoBM entityToDto(Turno entity){
        TurnoDtoBM dto = this.modelMapper.map(entity, TurnoDtoBM.class);
        dto.setMedico(this.medicoMapper.entityToDto(entity.getIdmedico()));
        dto.setPaciente(this.pacienteMapper.entityToDto(entity.getIdpaciente()));

        return dto;
    }

    public Turno updateEntityFromDto(TurnoDtoA turnoDtoA, Turno turnoEntity, Paciente pacienteEntity, Medico medicoEntity){
        turnoEntity.setIdpaciente(pacienteEntity);
        turnoEntity.setIdmedico(medicoEntity);
        turnoEntity.setFecha(turnoDtoA.getFecha());
        turnoEntity.setHora(turnoDtoA.getHora());
        turnoEntity.setAtendido(0);
        return turnoEntity;
    }
}
