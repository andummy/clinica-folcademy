package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "paciente")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    private Integer idpaciente;

    @Column(name = "dni", columnDefinition = "VARCHAR")
    private String dni;

    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    private String nombre;

    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    private String apellido;

    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    private String telefono;
}
