package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.ErrorMessages;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.TurnoDtoA;
import com.folcademy.clinica.Model.Dtos.TurnoDtoBM;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.aspectj.bridge.IMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final MedicoRepository medicoRepository;
    private final PacienteRepository pacienteRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, MedicoRepository medicoRepository, PacienteRepository pacienteRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.medicoRepository = medicoRepository;
        this.pacienteRepository = pacienteRepository;
        this.turnoMapper = turnoMapper;
    }

    @Override
    public List<TurnoDtoBM> findAll(){
        return this.turnoRepository.findAll().stream().map(this.turnoMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    public TurnoDtoBM updateTurno(TurnoDtoA turnoDto, Integer id) {
        Optional<Turno> turnoEntity = this.turnoRepository.findById(id);
        TurnoDtoBM response = null;

        //Consultar colocar la validacion del medico y paciente dentro del mapper (Relacionarse el mapper con los repo)
        if(turnoEntity.isPresent()){
            Optional<Medico> medicoEntity = this.medicoRepository.findById(turnoDto.getIdMedico());
            if(medicoEntity.isPresent()){
                Optional<Paciente> pacienteEntity = this.pacienteRepository.findById(turnoDto.getIdPaciente());
                if(pacienteEntity.isPresent()){
                    response = this.turnoMapper.entityToDto(
                            this.turnoRepository.save(
                                    this.turnoMapper.updateEntityFromDto(
                                            turnoDto, turnoEntity.get(),pacienteEntity.get(),medicoEntity.get()
                                    )
                            )
                    );
                }else {
                    throw new NotFoundException(ErrorMessages.PACIENTE_NOT_FOUND, "idPaciente: "+turnoDto.getIdPaciente());
                }
            }else {
                throw new NotFoundException(ErrorMessages.MEDICO_NOT_FOUND, "idMedico: "+turnoDto.getIdMedico().toString());
            }
        }else {
            throw new NotFoundException(ErrorMessages.TURNO_NOT_FOUND, "id: "+id.toString());
        }
        return response;
    }

    @Override
    public TurnoDtoBM createTurno(TurnoDtoA turnoDtoA) {
        Optional<Paciente> paciente = this.pacienteRepository.findById(turnoDtoA.getIdPaciente());
        Optional<Medico> medico = this.medicoRepository.findById(turnoDtoA.getIdMedico());
        TurnoDtoBM response = null;

        if(paciente.isPresent()){
            if (medico.isPresent()){
                response = this.turnoMapper.entityToDto(
                        this.turnoRepository.save(
                                this.turnoMapper.dtoAToEntity(turnoDtoA, paciente.get(), medico.get())
                        )
                );
            }else {
                System.out.println("lol");
                throw new NotFoundException(ErrorMessages.MEDICO_NOT_FOUND, turnoDtoA.getIdMedico().toString());
            }
        } else {
            throw new NotFoundException(ErrorMessages.PACIENTE_NOT_FOUND, turnoDtoA.getIdPaciente().toString());
        }
        return response;
    }

    @Override
    public ResponseEntity deleteTurno(Integer turnoId) {
        Optional<Turno> turnoEntity = this.turnoRepository.findById(turnoId);

        if (turnoEntity.isPresent()){
            this.turnoRepository.delete(turnoEntity.get());
        }else {
            throw new NotFoundException(ErrorMessages.TURNO_NOT_FOUND, "id : "+ turnoId.toString());
        }
        return new ResponseEntity<>("The entity has been successfully deleted", HttpStatus.OK);
    }
}
