package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.MedicoDtoABM;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IMedicoService {
    List<MedicoDtoABM> findAllMedicos();
    Medico createMedico(MedicoDtoABM dto);
    Medico updateMedico(MedicoDtoABM dto, Integer id);
    ResponseEntity deleteMedico(Integer id);
}

