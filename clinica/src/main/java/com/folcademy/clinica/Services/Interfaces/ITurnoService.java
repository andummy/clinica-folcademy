package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDtoA;
import com.folcademy.clinica.Model.Dtos.TurnoDtoBM;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ITurnoService {
    List<TurnoDtoBM> findAll();
    TurnoDtoBM updateTurno(TurnoDtoA turnoDto, Integer id);
    TurnoDtoBM createTurno(TurnoDtoA turnoDtoA);
    ResponseEntity deleteTurno(Integer TurnoId);
}
