package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.ErrorMessages;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDtoABM;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    @Override
    public List<PacienteDtoABM> findAllPacientes() {
        return this.pacienteRepository.findAll().stream().map(this.pacienteMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Paciente createPaciente(PacienteDtoABM dto) {
        return this.pacienteRepository.save(pacienteMapper.dtoToEntity(dto));
    }

    @Override
    public Paciente updatePaciente(PacienteDtoABM pacienteDto, Integer idPaciente) {
        Optional<Paciente> pacienteEntity = this.pacienteRepository.findById(idPaciente);
        if (pacienteEntity.isPresent()){
            this.pacienteMapper.updateEntityFromDto(pacienteDto,pacienteEntity.get());
            this.pacienteRepository.save(pacienteEntity.get());
        }else {
            throw new NotFoundException(ErrorMessages.PACIENTE_NOT_FOUND, "id: "+ idPaciente.toString());
        }
        return pacienteEntity.get();
    }

    @Override
    public ResponseEntity deletePaciente(Integer idPaciente) {
        Optional<Paciente> entity = this.pacienteRepository.findById(idPaciente);
        if (entity.isPresent()){
            this.pacienteRepository.delete(entity.get());
        }else {
            throw new NotFoundException(ErrorMessages.PACIENTE_NOT_FOUND, "id: "+ idPaciente.toString());
        }
        return new ResponseEntity<>("The entity has been successfully deleted", HttpStatus.OK);
    }
}
