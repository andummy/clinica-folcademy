package com.folcademy.clinica.Services.Security;

import com.folcademy.clinica.Model.Entities.Security.CustomClientDetails;
import com.folcademy.clinica.Model.Repositories.Security.UsuarioRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;
import static com.folcademy.clinica.Exceptions.ErrorMessages.USUARIO_CODIGO_NOT_FOUND;

@Service
public class CustomClientDetailsService implements ClientDetailsService {
    private final UsuarioRepository usuarioRepository;

    public CustomClientDetailsService(@Qualifier("usuarioRepository") UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public ClientDetails loadClientByClientId(String usuarioDesc) throws ClientRegistrationException {
        return new CustomClientDetails(
                usuarioRepository.findByUsuarioDesc(usuarioDesc)
                        .orElseThrow( () ->
                         new ClientRegistrationException(USUARIO_CODIGO_NOT_FOUND)
                        )
        );
    }

}

