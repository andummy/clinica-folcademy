package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.MedicoDtoABM;
import com.folcademy.clinica.Model.Dtos.PacienteDtoABM;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPacienteService {
    List<PacienteDtoABM> findAllPacientes();
    Paciente createPaciente(PacienteDtoABM dto);
    Paciente updatePaciente(PacienteDtoABM dto,Integer id);
    ResponseEntity deletePaciente(Integer id);
}
