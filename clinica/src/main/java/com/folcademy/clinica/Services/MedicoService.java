package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.ErrorMessages;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDtoABM;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService implements IMedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    @Override
    public List<MedicoDtoABM> findAllMedicos() {
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Medico createMedico(MedicoDtoABM dto) {
        return medicoRepository.save(medicoMapper.dtoToEntity(dto));
    }

    @Override
    public Medico updateMedico(MedicoDtoABM dto, Integer id) {
        Optional<Medico> entity = this.medicoRepository.findById(id);
        if(entity.isPresent()){
            this.medicoMapper.updateEntityFromDto(dto,entity.get());
            this.medicoRepository.save(entity.get());
        }else {
            throw new NotFoundException(ErrorMessages.MEDICO_NOT_FOUND, "id : "+id.toString());
        }
        return entity.get();
    }

    //Consultar sobre exepciones para refactorizar codigo
    @Override
    public ResponseEntity deleteMedico(Integer id){
        Optional<Medico> entity = medicoRepository.findById(id);
       if (entity.isPresent()){
            this.medicoRepository.delete(entity.get());
        }else {
            throw new NotFoundException(ErrorMessages.MEDICO_NOT_FOUND, "id : "+id.toString());
        }
        return new ResponseEntity<>("The entity has been successfully deleted", HttpStatus.OK);
    }
}
