package com.folcademy.clinica.Services.Security;

import com.folcademy.clinica.Model.Mappers.Security.UsuarioMapper;
import com.folcademy.clinica.Model.Repositories.Security.UsuarioRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static com.folcademy.clinica.Exceptions.ErrorMessages.USUARIO_DESC_NOT_FOUND;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final UsuarioRepository usuarioRepository;
    private final UsuarioMapper usuarioMapper;

    public CustomUserDetailsService(@Qualifier("usuarioRepository") UsuarioRepository usuarioRepository, @Qualifier("usuarioMapper") UsuarioMapper usuarioMapper) {
        this.usuarioRepository = usuarioRepository;
        this.usuarioMapper = usuarioMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String usuarioDesc) throws UsernameNotFoundException {
        return usuarioMapper.usuarioEntityToSpringSecurity(
                usuarioRepository.findByUsuarioDesc(usuarioDesc)
                        .orElseThrow( () ->
                                new UsernameNotFoundException(USUARIO_DESC_NOT_FOUND)
                        )
        );
    }
}
