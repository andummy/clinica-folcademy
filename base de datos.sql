-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: clinica-folcademy
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (1,'get',0,'2021-11-16 17:08:12','2021-11-16 17:08:12'),(2,'post',0,'2021-11-16 17:08:12','2021-11-16 17:08:12'),(3,'put',0,'2021-11-16 17:08:12','2021-11-16 17:08:12'),(4,'delete',0,'2021-11-16 17:08:12','2021-11-16 17:08:12');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorized_grant_types`
--

DROP TABLE IF EXISTS `authorized_grant_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorized_grant_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorized_grant_types`
--

LOCK TABLES `authorized_grant_types` WRITE;
/*!40000 ALTER TABLE `authorized_grant_types` DISABLE KEYS */;
INSERT INTO `authorized_grant_types` VALUES (1,'password',0,'2020-10-01 13:35:24','2020-10-01 13:35:24'),(2,'client_credentials',0,'2020-10-01 13:35:24','2020-10-01 13:35:24'),(3,'refresh_token',0,'2020-10-01 13:35:25','2020-10-01 13:35:25');
/*!40000 ALTER TABLE `authorized_grant_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medico` (
  `idmedico` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `profesion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `consulta` int DEFAULT NULL,
  PRIMARY KEY (`idmedico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES (1,'Ronald','Mc Donald','Cirujano',10000),(2,'Vin','Diesel','Ginecologo',2500),(3,'','','clinico',1500),(4,'Raton','Perez','Odontologo',500);
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `token` mediumblob,
  `authentication_id` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `authentication` mediumblob,
  `refresh_token` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_token`
--

LOCK TABLES `oauth_access_token` WRITE;
/*!40000 ALTER TABLE `oauth_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `token` mediumblob,
  `authentication` mediumblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_token`
--

LOCK TABLES `oauth_refresh_token` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paciente` (
  `idpaciente` int NOT NULL AUTO_INCREMENT,
  `dni` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apellido` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idpaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (1,'38595045','Ramiro','Bernal','2645473947'),(2,'40568974','Augusto','Giacone','264889977'),(3,'41255878','Maxi','Carubin','264895621');
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resources` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (1,'API_CLINICA',0,'2021-11-16 17:11:15','2021-11-16 17:11:15');
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scopes`
--

DROP TABLE IF EXISTS `scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scopes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scopes`
--

LOCK TABLES `scopes` WRITE;
/*!40000 ALTER TABLE `scopes` DISABLE KEYS */;
INSERT INTO `scopes` VALUES (1,'Read',0,'2021-11-16 17:10:30','2021-11-16 17:10:30'),(2,'Write',0,'2021-11-16 17:10:30','2021-11-16 17:10:30');
/*!40000 ALTER TABLE `scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turno` (
  `idturno` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `atendido` tinyint DEFAULT '0',
  `idpaciente` int DEFAULT NULL,
  `idmedico` int DEFAULT NULL,
  PRIMARY KEY (`idturno`),
  KEY `fk_medico_idx` (`idmedico`),
  KEY `fk_paciente_idx` (`idpaciente`),
  CONSTRAINT `fk_medico` FOREIGN KEY (`idmedico`) REFERENCES `medico` (`idmedico`),
  CONSTRAINT `fk_paciente` FOREIGN KEY (`idpaciente`) REFERENCES `paciente` (`idpaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turno`
--

LOCK TABLES `turno` WRITE;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
INSERT INTO `turno` VALUES (1,'2021-11-17','12:00:00',0,1,1);
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `usuarioId` int unsigned NOT NULL AUTO_INCREMENT,
  `usuarioCodigo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuarioClave` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuarioDesc` varchar(80) COLLATE utf8_spanish_ci DEFAULT '',
  `usuarioHabi` double(1,0) NOT NULL DEFAULT '0',
  `usuarioDelete` int NOT NULL DEFAULT '0',
  `accesstokenvalidityseconds` int NOT NULL DEFAULT '3600',
  `refreshtokenvalidityseconds` int NOT NULL DEFAULT '2629800',
  PRIMARY KEY (`usuarioId`),
  KEY `idx_acc_codigo` (`usuarioCodigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'0001','$2a$10$yC.xsA/G2.RUXfdf2aT6wemYl42iuzl0T/AUANUC7VCozOVwqAMou','admin',1,0,3600,2629800),(2,'0002','$2a$10$Z1SEqsJi4JRYAtD.Clbw.e8r7dleYuFJ3j5Mu3/KI8cSRtgMmbx.u','user',1,0,3600,2629800);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_authorities`
--

DROP TABLE IF EXISTS `usuarios_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_authorities` (
  `usuarioId` int unsigned NOT NULL,
  `id_authority` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_authority`),
  KEY `authority_fk` (`id_authority`),
  CONSTRAINT `authority_fk` FOREIGN KEY (`id_authority`) REFERENCES `authorities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_authorities`
--

LOCK TABLES `usuarios_authorities` WRITE;
/*!40000 ALTER TABLE `usuarios_authorities` DISABLE KEYS */;
INSERT INTO `usuarios_authorities` VALUES (1,1),(2,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `usuarios_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_authorized_grant_types`
--

DROP TABLE IF EXISTS `usuarios_authorized_grant_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_authorized_grant_types` (
  `usuarioId` int unsigned NOT NULL,
  `id_authorized_grant_types` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_authorized_grant_types`),
  KEY `authorized_grant_types_fk` (`id_authorized_grant_types`),
  CONSTRAINT `authorized_grant_types_fk` FOREIGN KEY (`id_authorized_grant_types`) REFERENCES `authorized_grant_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk4` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_authorized_grant_types`
--

LOCK TABLES `usuarios_authorized_grant_types` WRITE;
/*!40000 ALTER TABLE `usuarios_authorized_grant_types` DISABLE KEYS */;
INSERT INTO `usuarios_authorized_grant_types` VALUES (1,1),(2,1),(1,2),(2,2),(1,3),(2,3);
/*!40000 ALTER TABLE `usuarios_authorized_grant_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_resources`
--

DROP TABLE IF EXISTS `usuarios_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_resources` (
  `usuarioId` int unsigned NOT NULL,
  `id_resource` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_resource`),
  KEY `resource_fk` (`id_resource`),
  CONSTRAINT `resource_fk` FOREIGN KEY (`id_resource`) REFERENCES `resources` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk3` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_resources`
--

LOCK TABLES `usuarios_resources` WRITE;
/*!40000 ALTER TABLE `usuarios_resources` DISABLE KEYS */;
INSERT INTO `usuarios_resources` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `usuarios_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_scopes`
--

DROP TABLE IF EXISTS `usuarios_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_scopes` (
  `usuarioId` int unsigned NOT NULL,
  `id_scope` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_scope`),
  KEY `scope_fk` (`id_scope`),
  CONSTRAINT `scope_fk` FOREIGN KEY (`id_scope`) REFERENCES `scopes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk2` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_scopes`
--

LOCK TABLES `usuarios_scopes` WRITE;
/*!40000 ALTER TABLE `usuarios_scopes` DISABLE KEYS */;
INSERT INTO `usuarios_scopes` VALUES (1,1),(2,1),(1,2),(2,2);
/*!40000 ALTER TABLE `usuarios_scopes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-19 20:58:13